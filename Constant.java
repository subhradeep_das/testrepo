package com.dj.pim.constant;

public class Constant {
	
	public static final String CATALOG_NAME = "Product Catalog";
	public static final String INPUT_FIELD = "FilePath";
	public static final String MERCHANDISE_HIERARCHY = "Merchandise Hierarchy";
	public static final String BRAND_HIERARCHY = "Brand Hierarchy";
	public static final String LOGGER_NAME = "DemoLogger";
	public static final String MERCHANDISE_CATEGORY_ID ="Merchandise Hierarchy Spec/Category Number";
	public static final String MERCHANDISE_CATEGORY_NAME ="Merchandise Hierarchy Spec/Category Name";
	public static final String BRAND_CATEGORY_NAME ="Brand Hierarchy Spec/Category Description";
	public static final String BRAND_CATEGORY_ID ="Brand Hierarchy Spec/Category Code";
	
	public static final String VARIANT_ID_IN_SKU="SKU Spec/Variant";
	public static final String STYLE_ID_IN_SKU="Product Catalog Spec/Style";
	public static final String PRODUCT_TYPE="Product Catalog Spec/Product Type";
	public static final String STYLE_ID_IN_VARIANT="Product Catalog Spec/Style";
	public static String REFERENCE="Checking if item reference already present in parent";
	public static String GETTING_MAPPED="Item is getting mapped to parent item";
	public static String EXECUTING="Method started executing";
	public static String STYLE_ITEM="Style item corresponding to given Variant Item";
	public static String VARIANT_ITEM="Variant item corresponding to given SKU Item";
	public static String MAPPED_TO_PARENT="Item is mapped to parent item successfully";
	
}
